![Brasil317](http://d1k0tqnx0ph9lb.cloudfront.net/assets/images/logo.png?version=1490111691)

# Teste para vaga Desenvolvedor Web #


Este repositório é de uso exclusivo para o processo seletivo da vaga Desenvolvedor Web, da Brasil317.

### Objetivo ###
+ Por meio da realização do teste serão avaliados:
	* Domínio da tecnologia.
	* Arquitetura utilizada.
	* Performance.
	* Segurança.
	* Clareza e código limpo.
	* Design/Code patterns.
	* Entendimento do desafio.

### Briefing ###
+ Utilizar este repositório como a base para a realização do teste.
+ O contéudo do repositório consiste em:
	* index.html (HTML renderizado de uma página web).
	* /assets (estrutura básica de front-end: css/fonts/images).
+ Armazenar seu código em um repositório git (público).
+ Enviar e-mail para <recrutamento@brasil317.com.br>, quando concluir.

### Desafio ###
+ Transformar os dados da tabela em dados dinâmicos e aplicar o plugin  [DataTables](https://datatables.net/).
    * Back-end
	    * Criar uma aplicação em Laravel - 4.2 ou superior.
	    * Utilizar base de dados MySQL.
	    * Utilizar a classe Eloquent para leitura, escrita e validação dos dados.
	    * Criar Seed para preenchimento de dados fake.
	        * Utilizar estrutura da tabela de exemplo (index.html).
	    * Criar teste unitário para cobrir ao menos uma classe.
    * Front-end
	    * Efetuar os ajustes necessários para patterns de front-end.
	    * Obter os dados via back-end por meio de uma requisição AJAX.
	    * Exibir dados conforme tabela de exemplo.
	    * Implementar Busca e Paginação por meio do DataTables.
    * DevOps
	    * Será um diferencial rodar sua aplicação em container Docker.


### Dúvidas? ###
* Envie um e-mail para o nosso time de Recrutamento: <recrutamento@brasil317.com.br>.

## Boa sorte! ##
